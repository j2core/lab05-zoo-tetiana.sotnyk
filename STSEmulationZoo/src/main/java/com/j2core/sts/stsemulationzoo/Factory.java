package com.j2core.sts.stsemulationzoo;

import com.j2core.sts.stsemulationzoo.animal.*;
import com.j2core.sts.stsemulationzoo.animal.animalsinformation.Sex;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.LinkedList;

/**
* Class factory for created cages and animals in the zoo
*/
public class Factory {

    /**
     * Method create cage for Zoo
     *
     * @param cageClass  animal's class which live in this cage
     * @return object Cage
     * @throws NotImplementedException if this kind cage isn't specified in the factory
     */
    public static Cage createCage(Class cageClass)  throws NotImplementedException {

        switch (cageClass.getSimpleName()){
            case "Lion":
                return new Cage<>(new LinkedList<Lion>());
            case "Fox":
                return new Cage<>(new LinkedList<Fox>());
            case "Elephant":
                return new Cage<>(new LinkedList<Elephant>());
            case "Deer":
                return new Cage<>(new LinkedList<Deer>()) ;
            case "Bear":
                return new Cage<>(new LinkedList<Bear>());
            default:
                throw new NotImplementedException();
        }
    }


    /**
     * Method create Animals for cage in the Zoo
     *
     * @param animal  animal's class
     * @param name name Animals
     * @param sex sex Animals
     * @param age age Animals
     * @param normFood  norm food for Animals
     * @return object Animals
     * @throws NotImplementedException if this kind Animals isn't specified in the factory
     */
    public static Animal createAnimal(Class animal, String name, Sex sex, int age, int normFood ) throws NotImplementedException {

        switch (animal.getSimpleName()){
            case "Lion":
                return  new Lion(name, sex, age, normFood);
            case "Fox":
                return new Fox(name, sex, age, normFood);
            case "Elephant":
                return new Elephant(name, sex, age, normFood);
            case "Deer":
                return new Deer(name, sex, age, normFood);
            case "Bear":
                return new Bear(name, sex, age, normFood);
            default:
                throw new NotImplementedException();
        }
    }
}
