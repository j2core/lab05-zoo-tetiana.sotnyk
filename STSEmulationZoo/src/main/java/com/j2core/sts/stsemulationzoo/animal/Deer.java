package com.j2core.sts.stsemulationzoo.animal;

import com.j2core.sts.stsemulationzoo.animal.animalsinformation.Actions;
import com.j2core.sts.stsemulationzoo.animal.animalsinformation.Sex;
import com.j2core.sts.stsemulationzoo.animal.exception.DieAnimalException;

/**
 * Created by sts on 10/13/15.
 */
public class Deer extends Animal {

    public Deer(String name, Sex sex, int age, int normFood) {
        super(name, sex, age, normFood);
    }


    @Override
    public int behaveAnimal(int amountFood) throws DieAnimalException {

        lifeAtOneHour();
        if (fullness > 100){
            animalsAction(0, 0, 0, -5, Actions.SICK);
        } else
        if(fullness < 40){
            amountFood = eat(amountFood);
        } else
        if (sleepiness < 40){
            animalsAction(0, 15, -5, 10, Actions.SLEEPING);
        } else
        if (playfulness < 40){
            animalsAction(-5, -5, 30, 10, Actions.PLAYING);
        } else {
            amountFood = randomChoiceAction(amountFood);
        }

        determiningLivingStatusOfAnimal();
        return amountFood;
    }


    /**
     * The method random choice action for animal
     *
     * @param amountFood amount food in the cage
     * @return  amount food in the cage after eating Animals
     */
    public int randomChoiceAction (int amountFood){

        int choiceAction = ((int) (Math.random()*100)) % 5;
        switch (choiceAction){
            case 0:
            case 4:
                amountFood = eat(amountFood);
                break;
            case 1:
                animalsAction(0, 15, -5, 10, Actions.SLEEPING);
                break;
            case 2:
                animalsAction(-5, -5, 30, 10, Actions.PLAYING);
                break;
            case 3:
                animalsAction(0, 0, 0, 5, Actions.DOING_NOTHING);
                break;
        }
        return amountFood;
    }

}
