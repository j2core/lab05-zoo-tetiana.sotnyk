package com.j2core.sts.stsemulationzoo;

import com.j2core.sts.stsemulationzoo.animal.Animal;
import com.j2core.sts.stsemulationzoo.animal.exception.DieAnimalException;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by sts on 10/15/15.
 */
public class Cage<T extends Animal> {

    static List<Animal> dieAnimals = new LinkedList<>(); // List with die animals
    List<T> kind = null;                   // List with animals which live in this cage
    int amountFood = 0;                    // amount food in cage


    /**
     * Constructor object Cage
     *
     * @param kind  List with animals which live in this cage
     */
    public Cage(List<T> kind) {

        this.kind = kind;
    }


    @Override
    public String toString(){
        StringBuilder information = new StringBuilder();

        if(kind.size() > 0) {
            information.append(kind.get(0).getClass().getCanonicalName());
            information.append(System.lineSeparator());
            information.append(" Amount food for cage: ").append(amountFood);
            for (Animal aKind:kind) {
                information.append(aKind.toString());
                information.append(System.lineSeparator());
            }
            return information.toString();
        }
        else
            return "  In this cell there is no one";

    }


    /**
     * The method animate cage
     */
    public void animateCage() {
        if(kind.size() > 0){
            for(Animal aKind:kind){
                try {
                    amountFood = aKind.behaveAnimal(amountFood);
                }catch (DieAnimalException ex){
                    System.out.println(aKind.toString() + "  It's DIE!!!");
                    deleteOfAnimalFromCage(aKind);
                }
                System.out.println("");
            }
        }
    }


    /**
     * The method delete die Animals from cage
     *
     * @param dieAnimal died Animals
     */
    public void deleteOfAnimalFromCage(Animal dieAnimal){
        if(kind.size() > 0){
            for(Iterator<? extends Animal> iterator = kind.iterator(); iterator.hasNext();){
                Animal aKind  = iterator.next();
                if(aKind.equals(dieAnimal)){
                    dieAnimals.add(dieAnimal);
                    iterator.remove();
                }
            }
        }
    }
}
