package com.j2core.sts.stsemulationzoo.animal.exception;

/**
 * Created by sts on 10/15/15.
 */

/**
 * The exception class if animal is die
 */
public class DieAnimalException extends Exception {

    public DieAnimalException(String message){
        super(message);
    }

    public DieAnimalException(){

    }
}
