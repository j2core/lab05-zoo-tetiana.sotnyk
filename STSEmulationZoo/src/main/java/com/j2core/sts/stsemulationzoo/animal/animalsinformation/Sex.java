package com.j2core.sts.stsemulationzoo.animal.animalsinformation;

/**
 * Created by sts on 10/26/15.
 */
/**
 * Animal's sex
 */
public enum Sex {

    MALE,
    FEMALE
}
