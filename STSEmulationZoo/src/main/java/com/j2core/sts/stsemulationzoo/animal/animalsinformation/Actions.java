package com.j2core.sts.stsemulationzoo.animal.animalsinformation;

/**
 * Created by sts on 10/26/15.
 */

/**
 * Animal's actions
 */
public enum Actions {

        EATING,
        SLEEPING,
        PLAYING,
        DOING_NOTHING,
        WASHING,
        SWIMMING,
        SICK,
        NOT_FIND_FOOD,
    }
