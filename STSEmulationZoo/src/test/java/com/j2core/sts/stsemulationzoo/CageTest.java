package com.j2core.sts.stsemulationzoo;

import com.j2core.sts.stsemulationzoo.animal.Animal;
import com.j2core.sts.stsemulationzoo.animal.Lion;
import com.j2core.sts.stsemulationzoo.animal.animalsinformation.Sex;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by sts on 11/4/15.
 */
public class CageTest {


    @Test
    public void testDeleteOfAnimalFromCage(){

        Cage cage = Factory.createCage(Lion.class);

        cage.kind.add(Factory.createAnimal(Lion.class, "Leo", Sex.MALE, 5, 7000));
        cage.kind.add(Factory.createAnimal(Lion.class, "Lara", Sex.FEMALE, 3, 5500));
        cage.kind.add(Factory.createAnimal(Lion.class, "Misha", Sex.MALE, 9, 8000));

        Animal animalDie = Factory.createAnimal(Lion.class, "Misha", Sex.MALE, 9, 8000);

        cage.deleteOfAnimalFromCage(animalDie);

        Assert.assertFalse(cage.kind.contains(animalDie));

    }
}
