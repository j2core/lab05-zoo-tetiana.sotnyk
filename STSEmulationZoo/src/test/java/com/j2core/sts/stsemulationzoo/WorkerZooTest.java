package com.j2core.sts.stsemulationzoo;

import com.j2core.sts.stsemulationzoo.animal.*;
import com.j2core.sts.stsemulationzoo.animal.animalsinformation.Sex;
import org.testng.Assert;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by sts on 11/4/15.
 */
public class WorkerZooTest <T extends Animal>{

    public class AnimalInformation{

        public String name;
        public Sex sex;
        public int age;
        public int normFood;

        public AnimalInformation(String name, Sex sex, int age, int normFood) {
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.normFood = normFood;
        }
    }


    @BeforeGroups(groups = "cages")
    public List<Cage<? extends Animal>> createdCages(){

        Factory factory = new Factory();

        LinkedList<Cage<? extends Animal>> cages = new LinkedList<>();

        Class[] animalsClasses = {Lion.class, Fox.class, Elephant.class, Deer.class, Bear.class };
        int[] amountAnimalsOneClass = {3, 1, 2, 2, 0};
        List<AnimalInformation> animalsInformation = new ArrayList<>();
        animalsInformation.add(new AnimalInformation("Leo", Sex.MALE, 5, 7000));
        animalsInformation.add( new AnimalInformation("Lara", Sex.FEMALE, 3, 5500));
        animalsInformation.add(new AnimalInformation("Misha", Sex.MALE, 9, 8000));
        animalsInformation.add( new AnimalInformation("Patrisha", Sex.FEMALE, 3, 2000));
        animalsInformation.add(new AnimalInformation("Perl", Sex.FEMALE, 1, 6000));
        animalsInformation.add(new AnimalInformation("Misha", Sex.MALE, 9, 8000));
        animalsInformation.add(new AnimalInformation("Boris", Sex.MALE, 2, 15000));
        animalsInformation.add(new AnimalInformation("Patrisha", Sex.FEMALE, 3, 2000));
        animalsInformation.add( new AnimalInformation("Lara", Sex.FEMALE, 3, 5500));

        int indexAnimalInformation = 0;
        for (int i = 0; i < animalsClasses.length; i++){
            Cage cage = factory.createCage(animalsClasses[i]);
            for (int j = 0; j < amountAnimalsOneClass[i]; j++){
                AnimalInformation information = animalsInformation.get(indexAnimalInformation);
                cage.kind.add(factory.createAnimal(animalsClasses[i], information.name, information.sex, information.age, information.normFood));
                indexAnimalInformation++;
            }
            cages.add(cage);
        }

        return cages;

    }


    @Test(groups = "cages")
    public void testRequiredAmountFood(){

        List<Cage<? extends Animal>> cages = createdCages();
        int[] normAmountFood = {20500, 2000, 14000, 17000, 0};

        for (int i = 0; i < cages.size(); i++){

            Assert.assertTrue( normAmountFood[i] == WorkerZoo.requiredAmountFood(cages.get(i)));

        }

    }


    @Test(groups = "cages")
    public void testNumberAnimalsForZoo(){

        Assert.assertTrue(8 == WorkerZoo.numberAnimalsForZoo(createdCages()));

    }

    @DataProvider(name = "cleanCage")
    public Object[][] dataProviderDataOfAnimalsInCage(){
        return new Object[][]{
                {new int[]{25000, 3000, 15000, 17000, 2000}, 53500},
                {new int[]{20500, 2000, 14000, 17000, 0}, 53500},
                {new int[]{15000, 1000, 13000, 16000, 3000}, 53500}
        };
    }


    @Test(groups = "cages", dataProvider = "cleanCage")
    public void testCleaningCages(int[] amountFoodInCages, int requiredAmountFood){

        List<Cage<? extends Animal>> cages = createdCages();

        int index = 0;
        for (Cage<? extends Animal> cage: cages){
            cage.amountFood = amountFoodInCages[index];
            index++;
        }

        WorkerZoo.cleaningCages(cages);

        int amountFoodAfterClean = 0;
        for (Cage<? extends Animal> cage: cages){
            amountFoodAfterClean = amountFoodAfterClean + cage.amountFood;
        }

        Assert.assertTrue(amountFoodAfterClean <= requiredAmountFood);

    }

}
