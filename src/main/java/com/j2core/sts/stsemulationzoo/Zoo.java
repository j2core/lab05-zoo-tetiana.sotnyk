package com.j2core.sts.stsemulationzoo;

import com.j2core.sts.stsemulationzoo.animal.*;
import com.j2core.sts.stsemulationzoo.animal.animalsinformation.Sex;
import org.apache.log4j.Logger;

import java.util.LinkedList;
import java.util.List;

/**
* Zoo works emulation
*/
public class Zoo {

    public static final Logger logger = Logger.getLogger(Zoo.class);
    private static  final int AMOUNT_WORKED_DAYS = 15;
    private static Class[] animalsClasses = {Lion.class, Fox.class, Elephant.class, Deer.class, Bear.class};
    private static int[] amountAnimalsInCage = {3, 2, 1, 2, 1};
    private static String[] animalsNames = {"Leo", "Lara", "Liza", "Olga", "Anton", "Boris", "Perl", "Storm", "Misha"};
    private static Sex[] animalsSex = {Sex.MALE, Sex.FEMALE, Sex.FEMALE, Sex.FEMALE, Sex.MALE, Sex.MALE, Sex.FEMALE, Sex.MALE, Sex.MALE };
    private static int[] animalsAge = {5, 4, 1, 3, 2, 10, 2, 2, 8};
    private static int[] animalsNormFood = {7000, 5500, 4000, 1500, 2000, 20000, 10000, 12000, 9000};
    private static int index = 0;


    public static void main( String[] args ) throws Exception {

        // Created list cages for cages
        List<Cage<? extends Animal>> cages = new LinkedList<>();

        for (int i = 0; i < animalsClasses.length; i++){

            Cage cage = Factory.createCage(animalsClasses[i]);

            for (int j = 0; j < amountAnimalsInCage[i]; j++){

                cage.kind.add(Factory.createAnimal(animalsClasses[i], animalsNames[index], animalsSex[index], animalsAge[index], animalsNormFood[index]));
                index++;
            }
            cages.add(cage);
        }

        // Determination of the number of animals at the beginning of the cages
        int numberAnimalsZooOld = WorkerZoo.numberAnimalsForZoo(cages);

        // Work cages
        for(int day = 1; day < AMOUNT_WORKED_DAYS; day++) {
            StringBuilder informationZoo = new StringBuilder();
            informationZoo.append(" Good morning ").append(WorkerZoo.nameWorker).append(". You work ").append(day).append(" day");
            informationZoo.append(System.lineSeparator());
            informationZoo.append("---------------------------------------------");
            informationZoo.append(System.lineSeparator());

            logger.info(informationZoo.toString());

            WorkerZoo.animateWorker(cages);

            logger.info("  " + WorkerZoo.nameWorker + ". Thank you for job of day");

        }

        //Determination of the number of animals at the end of the cages
        int numberAnimalsZooNew = WorkerZoo.numberAnimalsForZoo(cages);

        // Determination of the quality of the cages, depending on the amount of the remaining animals at the end of the cages
        if(numberAnimalsZooNew != numberAnimalsZooOld){
            if(numberAnimalsZooNew < numberAnimalsZooOld/2){
                logger.info("  " + WorkerZoo.nameWorker + ",  very bad job!!!");

            }else
            if(numberAnimalsZooNew > (numberAnimalsZooOld*0.75)) {
                logger.info("  " + WorkerZoo.nameWorker + ", good job. Thank you.");

            }
            else logger.info("  " + WorkerZoo.nameWorker + ", thank you for job.");
        }
        else logger.info("  " + WorkerZoo.nameWorker + ",  very good job!!! Thank you!!!");
    }

}
