package com.j2core.sts.stsemulationzoo.animal;

import com.j2core.sts.stsemulationzoo.animal.animalsinformation.Actions;
import com.j2core.sts.stsemulationzoo.animal.animalsinformation.Sex;
import com.j2core.sts.stsemulationzoo.animal.exception.DieAnimalException;
import org.apache.log4j.Logger;

/**
 * Created by sts on 10/13/15.
 */
abstract public class Animal {

    protected String name;             // animal's name
    protected String foodType;         // food typeCage Animals
    Sex sex;                           // animal's sex
    int age;                           // animal's age
    public final int normFood;         // normal amount of food for the animal per day
    protected int fullness = 75;       // parameter fullness Animals
    protected int playfulness = 75;    // parameter playfulness Animals
    protected int sleepiness = 75;     // parameter sleepiness Animals
    protected int happiness = 100;     // parameter happiness Animals
    public static final Logger logger = Logger.getLogger(Animal.class);


    /**
     * Constructor object Animals
     *
     * @param name   animal's name
     * @param sex    animal's sex
     * @param age    animal's age
     * @param normFood  normal amount of food for the animal per day
     */
    public Animal(String name, Sex sex, int age, int normFood) {
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.normFood = normFood;
    }


    /**
     * The method of performing parameter happiness change by the specified amount
     *
     * @param deltaFun amount by which the option is to change the parameter happiness
     */
    public int changeParameterFun(int deltaFun){
        happiness = happiness + deltaFun;
        if(happiness > 100)  happiness = 100;
        return happiness;
    }



    /**
     * The method of performing parameter sleepiness change by the specified amount
     *
     * @param deltaSleep amount by which the option is to change the parameter sleepiness
     */
    public int changeParameterSleep(int deltaSleep){
        sleepiness = sleepiness + deltaSleep;
        return sleepiness;
    }


    /**
     * The method of performing parameter satiety change by the specified amount
     *
     * @param deltaFullness amount by which the option is to change the parameter satiety
     */
    public int changeParameterFullness(int deltaFullness){
        fullness = fullness + deltaFullness;
        return fullness;
    }


    /**
     * The method of performing parameter playfulness change by the specified amount
     *
     * @param deltaPlayfulness amount by which the option is to change the parameter playfulness
     */
    public int changeParameterPlayfulness(int deltaPlayfulness){
        playfulness = playfulness + deltaPlayfulness;
        return playfulness;
    }


    /**
     * The method which performs emulation living Animals, regardless of its action.
     *
     * Change all the parameters of the Animals per hour regardless of its action.
     */
    public void lifeAtOneHour(){

        changeParameterFullness(-4);
        changeParameterSleep(-4);
        changeParameterPlayfulness(-4);
        changeParameterFun(-4);
    }


    /**
     * Method to simulate the actions of an Animals. And depending on the action which he now performs changes the basic
     * parameters of the Animals to certain values. With the withdrawal of the information which action is now performed
     * by the Animals
     *
     * @param deltaFullness    The number on which you want to change the parameter value fullness
     * @param deltaSleepiness  The number on which you want to change the parameter value sleepiness
     * @param deltaPlayfulness The number on which you want to change the parameter value playfulness
     * @param deltaHappiness   The number on which you want to change the parameter value fun
     * @param action           Name of the action that performs Animals from enum
     */
    public void animalsAction(int deltaFullness, int deltaSleepiness, int deltaPlayfulness, int deltaHappiness, Actions action) {
        changeParameterFullness(deltaFullness);
        changeParameterSleep(deltaSleepiness);
        changeParameterPlayfulness(deltaPlayfulness);
        changeParameterFun(deltaHappiness);
        if (action == Actions.NOT_FIND_FOOD){
            logger.info("  " + getClass().getSimpleName() + " " + name + " is hungry, but not find food!");
        }else logger.info(" " + getClass().getSimpleName() +  " " + name + " at the present moment is " + action);

    }


    /**
     * Method checks the Animal's living conditions
     *
     * @throws DieAnimalException
     */
    public void determiningLivingStatusOfAnimal() throws DieAnimalException {
        if (fullness <= 0 || sleepiness <= 0 || playfulness <= 0 || happiness <= 0){
            throw new DieAnimalException(" This Animals is die!");
        }
    }


    /**
     * The method implement eat Animals
     *
     * @param amountFood amount food in the cage
     * @return  amount food in the cage after eating Animals
     */
    public int eat(int amountFood){

        int proCentFoundFood;

        if (amountFood > 0){

            proCentFoundFood = (int) (Math.random() * 100);
            if (proCentFoundFood < 2){
                proCentFoundFood = 2;
            }
            int amountFoundFood = normFood * proCentFoundFood / 100;

            if (amountFood >= amountFoundFood){
                animalsAction(proCentFoundFood / 2, -2, -2, 10, Actions.EATING);
                amountFood = amountFood - amountFoundFood;
            }
            else {
                proCentFoundFood  = amountFood / normFood * 100;
                if (proCentFoundFood < 2){
                    proCentFoundFood = 2;
                }
                animalsAction(proCentFoundFood / 2, -2, -2, 5, Actions.EATING);
                amountFood = 0;
            }

        }else {
            animalsAction(0, -2, -2, -5, Actions.NOT_FIND_FOOD);
        }
        return amountFood;
    }


    /**
     * The method behave animals
     *
     * @param amountFood amount food in the cage
     * @return amount food in the cage after one hour lived
     * @throws DieAnimalException if Animal's parameters < parameter's min, Animals is die
     */
    abstract public int behaveAnimal(int amountFood) throws DieAnimalException;


    @Override
    public String toString(){

        StringBuilder information = new StringBuilder();

        information.append("  ").append(getClass().getSimpleName()).append(", Type food: ").append(foodType).append(".").append(System.lineSeparator());
        information.append("  Name: ").append(name).append(", Sex: ").append(sex).append(", Age: ").append(age).append(", Norm food: ").append(normFood).append(".");
        information.append(System.lineSeparator()).append("  Fullness: ").append(fullness).append(", Playfulness: ").append(playfulness);
        information.append(", Sleepiness: ").append(sleepiness).append(", Happiness: ").append(happiness).append(".");

        return information.toString();
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(this.getClass() == obj.getClass()))return false;

        Animal animal = (Animal) obj;

        if (age != animal.age) return false;
        if (normFood != animal.normFood) return false;
        if (!name.equals(animal.name)) return false;
        return sex == animal.sex;

    }


    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + sex.hashCode();
        result = 31 * result + age;
        result = 31 * result + normFood;
        return result;
    }

}
