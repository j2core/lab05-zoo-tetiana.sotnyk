package com.j2core.sts.stsemulationzoo;

import com.j2core.sts.stsemulationzoo.animal.Animal;
import org.apache.log4j.Logger;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 * Created by sts on 10/16/15.
 * Class for animation worker Zoo
 */
public class WorkerZoo {

    private static final int AMOUNT_HOURS_IN_DAY = 24;
    private static final Logger logger = Logger.getLogger(WorkerZoo.class);
    static String nameWorker = "Tetiana";

    /**
     * The method animate worker animalsCages
     *
     * @param cages List with cage in the animalsCages
     */
    public  static void animateWorker(List<Cage<? extends Animal>> cages){

        // Report about animals in the animalsCages
        logger.info("  Animals in the animalsCages:");

        for (Cage cage : cages){
            logger.info(cage.toString());
        }
        if (Cage.dieAnimals.size() > 0) {
            logger.info("  Died animals: ");
            logger.info(Cage.dieAnimals.toString());
        }else
            logger.info("  No dead animals");

        //Cleaning cage
        cleaningCages(cages);

        //Feeding animals
        feeding(cages);

        //Supervision for animals
        logger.info("  " + nameWorker + " watch the animals.");
        for (int i = 0; i< AMOUNT_HOURS_IN_DAY; i++){
            int time;
            if (i < 16){
                time = i + 8;
            }
            else time = i - 16;
            logger.info("  At " + time + ":00 ");

            for (Cage cage : cages){
                cage.animateCage();
            }
        }

    }


    /**
     * The method calculation number animals in the Zoo
     *
     * @param cages List with cage in the animalsCages
     * @return number animals in the Zoo
     */
    public  static int numberAnimalsForZoo(List<Cage<? extends Animal>> cages){
        int number = 0;
        for (Cage<? extends Animal> cage: cages){
            number = number + cage.kind.size();
        }
        return number;

    }


    /**
     * The method feeding animals in thr Zoo
     *
     * @param animalsCages List with cage in the animalsCages
     */
    public static void feeding(List<Cage<? extends Animal>> animalsCages) {

//        Scanner scanner = new Scanner(System.in);

        logger.info("  " + nameWorker + ", this is time to feeding animals");

        for (Cage<? extends Animal> cage : animalsCages) {
            if (cage.kind.size() > 0) {

                int amountFoodInCage = cage.amountFood;
                while (amountFoodInCage == cage.amountFood) {
                    try {

                        System.out.println(" Enter amount food for " + cage.kind.get(0).getClass().getSimpleName() + " cage.");
                        System.out.println(" The recommended amount of food for this cage - " + requiredAmountFood(cage) + ".");

                        Scanner scanner = new Scanner(System.in);
                        cage.amountFood = scanner.nextInt();

                        if (cage.amountFood == amountFoodInCage) throw new InputMismatchException();

                    } catch (InputMismatchException ex) {
                        System.out.println(" Character is entered incorrectly, please try it again!");
                    }
                }
            }
        }

    }


    /**
     * The method clean cages
     *
     * @param cages List with cage in the animalsCages
     */
    public static void cleaningCages(List<Cage<? extends Animal>> cages){

        for (Cage<? extends Animal> cage : cages) {
            if (cage.amountFood > requiredAmountFood(cage)) {
                cage.amountFood = 0;
                if (cage.kind.size() > 0) {
                    logger.info("  Cage " + cage.kind.get(0).getClass().getSimpleName() + " cleaned.");
                }
            }
        }
    }


    /**
     * The method calculation required amount food for cage
     *
     * @param cage cage with animals
     * @return required amount food for this cage
     */
    public static int requiredAmountFood(Cage<? extends Animal> cage){

        int reqAmFoodCage = 0;
        for (Animal animal : cage.kind ){
            reqAmFoodCage = reqAmFoodCage + animal.normFood;
        }
        return reqAmFoodCage;
    }

}
