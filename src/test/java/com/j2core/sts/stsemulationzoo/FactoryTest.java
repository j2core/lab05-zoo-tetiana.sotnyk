package com.j2core.sts.stsemulationzoo;

import com.j2core.sts.stsemulationzoo.animal.*;
import com.j2core.sts.stsemulationzoo.animal.animalsinformation.Sex;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.lang.reflect.InvocationTargetException;


/**
* Created by sts on 10/27/15.
*/
public class FactoryTest {


    @Test(expectedExceptions = NotImplementedException.class)
    public void testCreateCageNegativeCase() throws NotImplementedException {

        Factory.createCage(Cage.class);
    }


    @DataProvider(name = "createCage&Animals")
    public Object[][] dataProviderCreateData(){
        return new Object[][]{
                {Bear.class, "Ivan", Sex.MALE, 8, 10000},
                {Deer.class, "Perl", Sex.FEMALE, 1, 6000},
                {Elephant.class, "Boris", Sex.MALE, 2, 15000},
                {Fox.class, "Patrisha", Sex.FEMALE, 3, 2000},
                {Lion.class, "Lara", Sex.FEMALE, 3, 5500},
                {Lion.class, "Leo", Sex.MALE, 5, 7000},
        };
    }


    @Test(dataProvider = "createCage&Animals")
    public void testCreateCagePositiveCase(Class cageClass, String name, Sex sex, int age, int normFood) throws NotImplementedException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {

        Cage cage =  Factory.createCage(cageClass);

        Assert.assertTrue(cage.kind.add(Factory.createAnimal(cageClass, name, sex, age, normFood)));

    }


    @Test(dataProvider = "createCage&Animals", groups = "factory")
    public void testCreateAnimalsPositiveCase(Class animalsClass, String nameAnimal, Sex sex, int age, int normFood) throws NotImplementedException {

        Assert.assertTrue(Factory.createAnimal(animalsClass, nameAnimal, sex, age, normFood).getClass().equals(animalsClass));

    }


    @Test(expectedExceptions = Exception.class, groups = "factory")
    public void testCreateAnimalsNegativeCase() throws Exception {

        Factory.createAnimal(Cage.class, "Aly", Sex.FEMALE, 8, 250);
    }

}
