package com.j2core.sts.stsemulationzoo;

import com.j2core.sts.stsemulationzoo.animal.*;
import com.j2core.sts.stsemulationzoo.animal.animalsinformation.Sex;
import com.j2core.sts.stsemulationzoo.animal.exception.DieAnimalException;
import org.testng.Assert;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


/**
* Created by sts on 10/27/15.
*/
public class AnimalTest {


    @DataProvider(name = "hashCode&equalsPositive")
    public Object[][] dataProviderHashCodePositive(){
        return new Object[][]{
                { "Leo", Sex.MALE, 5, 7000, Lion.class},
                {"Lara", Sex.FEMALE, 3, 5500, Lion.class},
                {"Perl", Sex.FEMALE, 1, 6000, Deer.class},
                {"Misha", Sex.MALE, 9, 8000, Bear.class},
                {"Patrisha", Sex.FEMALE, 3, 2000, Fox.class},
                {"Boris", Sex.MALE, 2, 15000, Elephant.class},
        };
    }


    @DataProvider(name = "equalsNegative")
    public Object[][] dataProviderHashCodeNegative(){
        return new Object[][]{
                {Elephant.class, "Boris", Sex.MALE, 2, 15000,"Leo", Sex.MALE, 5, 7000},
                {Lion.class, "Leo", Sex.MALE, 5, 7000, "Misha", Sex.MALE, 9, 8000},
                {Lion.class, "Lara", Sex.FEMALE, 3, 5500, "Perl", Sex.FEMALE, 1, 6000},
                {Deer.class, "Perl", Sex.FEMALE, 1, 6000, "Misha", Sex.MALE, 9, 8000},
                {Bear.class, "Misha", Sex.MALE, 9, 8000, "Leo", Sex.MALE, 5, 7000},
                {Fox.class, "Patrisha", Sex.FEMALE, 3, 2000, "Perl", Sex.FEMALE, 1, 6000},
        };
    }


    @Test(dataProvider = "hashCode&equalsPositive")
    public void testHashCode(String nameAnimal, Sex sex, int age, int normFood, Class<Animal> animalsClass) throws Exception {

        Animal animal1 = animalsClass.getConstructor(String.class, Sex.class, int.class, int.class).newInstance(nameAnimal, sex, age, normFood);
        Animal animal2 = animalsClass.getConstructor(String.class, Sex.class, int.class, int.class).newInstance(nameAnimal, sex, age, normFood);

        Assert.assertEquals(animal1.hashCode(), animal2.hashCode());
    }


    @Test(dataProvider = "hashCode&equalsPositive")
    public void testEqualsPositiveCase(String nameAnimal, Sex sex, int age, int normFood, Class<Animal> animalsClass) throws Exception{

        Animal animal1 = animalsClass.getConstructor(String.class, Sex.class, int.class, int.class).newInstance(nameAnimal, sex, age, normFood);
        Animal animal2 = animalsClass.getConstructor(String.class, Sex.class, int.class, int.class).newInstance(nameAnimal, sex, age, normFood);

        Assert.assertTrue(animal1.equals(animal2));

    }


    @Test(dataProvider = "equalsNegative")
    public void testEqualsNegativeCase(Class<Animal> animalsClass, String nameAnimal1, Sex sex1, int age1, int normFood1, String nameAnimal2, Sex sex2, int age2, int normFood2) throws Exception {

        Animal animal1 = animalsClass.getConstructor(String.class, Sex.class, int.class, int.class).newInstance(nameAnimal1, sex1, age1, normFood1);
        Animal animal2 = animalsClass.getConstructor(String.class, Sex.class, int.class, int.class).newInstance(nameAnimal2, sex2, age2, normFood2);

        Assert.assertFalse(animal1.equals(animal2));
    }


    @BeforeGroups(groups = "changeParameterForAnimal")
    public Animal createdTestAnimal(){

        return new Lion("Leo", Sex.MALE, 5, 7000);

    }


    @DataProvider(name = "changeParameterFun")
    public Object[][] dataProviderChangeParameterFun(){
        return new Object[][]{
                {100, 10},
                {75, -25},
                {100, 0},
                {100, -0},
                {0, -100},
        };
    }


    @Test(dataProvider = "changeParameterFun", groups = "changeParameterForAnimal")
    public void testChangeParameterFun(int result, int delta){

        Assert.assertEquals(result, createdTestAnimal().changeParameterFun(delta));
    }


    @DataProvider(name = "changeParameters")
    public Object[][] dataProviderChangeParameters(){
        return new Object[][]{
                {75, 0},
                {75, -0},
                {65, -10},
                {85, 10},
                {0, -75},
                {110, 35}
        };
    }


    @Test(dataProvider = "changeParameters", groups = "changeParameterForAnimal")
    public  void testChangeParameterSleep(int result, int delta){

        Assert.assertEquals(result, createdTestAnimal().changeParameterSleep(delta));
    }


    @Test(dataProvider = "changeParameters", groups = "changeParameterForAnimal")
    public  void testChangeParameterPlayFullness(int result, int delta){

        Assert.assertEquals(result, createdTestAnimal().changeParameterPlayfulness(delta));
    }


    @Test(dataProvider = "changeParameters", groups = "changeParameterForAnimal")
    public  void testChangeParameterFullness(int result, int delta){

        Assert.assertEquals(result, createdTestAnimal().changeParameterFullness(delta));
    }


    @DataProvider(name = "determiningLivingStatusOfAnimal")
    public Object[][] dataProviderStateOfHealthAnimal(){
        return new Object[][]{
                {0, 0, 0, -100},
                {0, 0, -75, 0},
                {0, -75, 0, 0},
                {-75, 0, 0, 0},
                {-75, -75, -75, -100},
                {-100, -100, -100, -150}
        };
    }


    @Test(groups = "definitionParameterForAnimal", dataProvider = "determiningLivingStatusOfAnimal", expectedExceptions = DieAnimalException.class)
    public void testDeterminingLivingStatusOfAnimal(int deltaFullness, int deltaSleep, int deltaPlay, int deltaFun) throws DieAnimalException {

        Animal animal = createdTestAnimal();

        animal.changeParameterFullness(deltaFullness);
        animal.changeParameterSleep(deltaSleep);
        animal.changeParameterPlayfulness(deltaPlay);
        animal.changeParameterFun(deltaFun);

        animal.determiningLivingStatusOfAnimal();

    }


    @DataProvider(name = "eat")
    public Object[][] dataProviderEating(){
        return new Object[][]{
                {createdTestAnimal().normFood},
                {createdTestAnimal().normFood/2},
                {0}
        };
    }


    @Test(groups = "changeParameterForAnimal", dataProvider = "eat")
    public void testEat(int amountFood){

        Animal animal = createdTestAnimal();

        animal.eat(amountFood);
        if (amountFood > 0){
            Assert.assertFalse(animal.changeParameterFullness(0) == 75);
        }else
        Assert.assertTrue(animal.changeParameterFullness(0) == 75);

    }

}
